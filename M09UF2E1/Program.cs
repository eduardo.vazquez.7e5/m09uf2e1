﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace M09UF2E1
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            List<Task> tasks = new List<Task>();
            Object theBible = null;
            tasks.Add(Task.Factory.StartNew(() => theBible = FileReader.CountFileLines("The Bible.txt")));
            Object novum = null;
            tasks.Add(Task.Factory.StartNew(() => novum = FileReader.CountFileLines("Novum organon renovatum.txt")));
            Object pigs = null;
            tasks.Add(Task.Factory.StartNew(() => pigs = FileReader.CountFileLines("THE STORY OF THE THREE LITTLE PIGS.txt")));
            while (tasks.Any(t => !t.IsCompleted)) { }
            Console.WriteLine("\nThe Bible has " + theBible + " lines\nNovum Organon Renovatum has " + novum + " lines\nThe Three Little Pigs has " + pigs + " lines");
        }
    }
}