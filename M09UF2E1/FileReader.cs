using System;
using System.IO;

namespace M09UF2E1
{
    [Serializable]
    public class InvalidPath : Exception
    {
        public InvalidPath(string path) : base("INVALID PATH: " + path) { }
    }
    
    public class FileReader
    {
        public static int CountFileLines(string path)
        {
            string totalPath = Path.GetFullPath(Directory.GetCurrentDirectory() + "/../../../Files/" + path);
            if (File.Exists(totalPath))
            {
                string text = File.ReadAllText(totalPath);
                int result = text.Split('\n').Length - 1;
                Console.WriteLine("FINISHED READING FILE: " + path);
                return result;
            }
            throw new InvalidPath(totalPath);
        }
    }
}